# SSBB ENVIRONMENT

Proyecto para generar deploy de manera más rapida, ya que utiliza git submodules para agrupar todos los repositorios en un mismo proyecto.

## DEPLOY

Para realizar el deploy del ambiente ingresar el siguiente comando:

```sh
$ git clone --recursive <git-repository> apps
$ cd apps
$ docker-compose up -d --build
```

Para realizar el deploy de un solo servicio, modificar ultima linea:

```sh
$ git clone --recursive <git-repository> apps
$ cd apps
$ docker-compose up -d --build <service>
```

## ADD

Para añadir más repositorios a este proyecto, ejecutar el siguiente comando:

```sh
$ cd <project>
$ git submodule add <git-repository>

# add with branch
$ git submodule add -b <branch>  <git-repository>
```
